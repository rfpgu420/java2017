import spi.*;

public class FindMaxPlugin implements Processable {
    
    @Override
    public void process(ProcessContext ctx) {
        int[][] elements = ctx.getElements();
        
        int maxElement = elements[0][0];
        for (int[] row: elements) {
            for (int e: row) {
                maxElement = (e > maxElement) ? e : maxElement;
            }
        }
        System.out.format("Max element: %d%n", maxElement);
    }
}