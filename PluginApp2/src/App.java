public class App {
    
    private static int[][] elements = {
        {78, 89, 34, 12, 45, 23, 9, 1, 56},
        {37, 29, 23, 12, 2, 89, 30, 27},
        {67, 39, 3, 9, 18, 36, 15},
        {99, 24},
        {76, 82, 35, 39, 26, 71, 4},
        {29, 58, 41, 93, 85, 11, 78, 29}
    };

    private static enum Method {
        MIN("MIN"){
            @Override
            public void process(String method) {
                if (! isSuitable(method)) {
                    return;
                }
                printElements();

                int minElement = elements[0][0];
                for (int[] row: elements) {
                    for (int e: row) {
                        minElement = (e < minElement) ? e : minElement;
                    }
                }
                System.out.format("Min element: %d%n", minElement);
            }
        },
        MAX("MAX"){
            @Override
            public void process(String method) {
                if (! isSuitable(method)) {
                    return;
                }
                printElements();

                int maxElement = elements[0][0];
                for (int[] row: elements) {
                    for (int e: row) {
                        maxElement = (e > maxElement) ? e : maxElement;
                    }
                }
                System.out.format("Max element: %d%n", maxElement);
            }
        },
        SUM("SUM"){
            @Override
            public void process(String method) {
                if (! isSuitable(method)) {
                    return;
                }
                printElements();

                int sumElements = 0;
                for (int[] row: elements) {
                    for (int e: row) {
                        sumElements += e;
                    }
                }
                System.out.format("Sum of elements: %d%n", sumElements);
            }
        },
        ROWMIN("ROWMIN"){
            @Override
            public void process(String method) {
                if (! isSuitable(method)) {
                    return;
                }
                printElements();

                int minElement;
                for (int i = 0; i < elements.length; i++) {
                    minElement = elements[i][0];
                    for (int e: elements[i]) {
                        minElement = (e < minElement) ? e : minElement;
                    }
                    System.out.format("row (%d) min: %d%n", i, minElement);
                }
            }
        },
        ROWMAX("ROWMAX"){
            @Override
            public void process(String method) {
                if (! isSuitable(method)) {
                    return;
                }
                printElements();

                int maxElement;
                for (int i = 0; i < elements.length; i++) {
                    maxElement = elements[i][0];
                    for (int e: elements[i]) {
                        maxElement = (e > maxElement) ? e : maxElement;
                    }
                    System.out.format("row (%d) max: %d%n", i, maxElement);
                }
            }
        },
        ROWSUM("ROWSUM"){
            @Override
            public void process(String method) {
                if (! isSuitable(method)) {
                    return;
                }
                printElements();

                int rowSum;
                for (int i = 0; i < elements.length; i++) {
                    rowSum = 0;
                    for (int e: elements[i]) {
                        rowSum += e;
                    }
                    System.out.format("row (%d) sum: %d%n", i, rowSum);
                }
            }
        };

        /**
         * Processing method.
         */
        private final String method;

        /**
         * Non instantiable constructor.
         * 
         * @param  method [description]
         * @return        [description]
         */
        private Method(String method) {
            this.method = method;
        }

        /**
         * [printElements description]
         */
        public void printElements(){
            for (int i=0; i < elements.length; i++) {
                for (int j=0; j < elements[i].length; j++) {
                    System.out.format("%d \t", elements[i][j]);
                }
                System.out.println();
            }
        }

        /**
         * Check processing method.
         * 
         * @param  method  input method
         * @return         [description]
         */
        public boolean isSuitable(String method) {
            return this.method.toUpperCase().equals(method.toUpperCase());
        }

        /**
         * Execute routines.
         * 
         * @param method  input method
         */
        abstract public void process(String method);
    };


    /**
     * [process description]
     * 
     * @param  [parameter-name] [description]
     * @return [description]
     */
    private static void process(String mode) {
        for (Method m: Method.values()) {
            m.process(mode);
        }
    }

    /**
     * [main description]
     * 
     * @param args [description]
     *
     * args:  { min | max | sum | rowmax | rowmin | rowsum}
     */
    public static void main(String[] args)
    {
        if (args.length != 1) {
            System.out.println("Error: have no mode!");
            System.out.println("The valid usages:");
            System.out.println("java App min");
            System.out.println("java App max");
            System.out.println("java App sum");
            System.out.println("java App rowmin");
            System.out.println("java App rowmax");
            System.out.println("java App rowsum");
            return;
        }
        process(args[0]);
    }
}