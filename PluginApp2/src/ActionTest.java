import java.util.*;
import java.io.*;

import spi.*;

public class ActionTest {

    // the directory where we keep the plugin classes
    String pluginsDir;

    // a list where we keep an initialized object of each plugin class
    List plugins;


    private static int[][] elements = {
        {78, 89, 34, 12, 45, 23, 9, 1, 56},
        {37, 29, 23, 12, 2, 89, 30, 27},
        {67, 39, 3, 9, 18, 36, 15},
        {99, 24},
        {76, 82, 35, 39, 26, 71, 4},
        {29, 58, 41, 93, 85, 11, 78, 29}
    };
    
    
    public static void main (String args[]) {
        ActionTest demo = new ActionTest(args);
        demo.getPlugins();
        demo.runPlugins();
    }

    public ActionTest (String args[])
    {
        pluginsDir = (args.length > 0) ? args[0] : "ext";
        plugins    = new ArrayList();
        
        System.setSecurityManager(new PluginSecurityManager(pluginsDir));
    }

    private static Set<Class<?>> getAllInterfaces(Class<?> clazz, Object old)
    {
        List<Class<?>> res = new ArrayList<>();
        do {
            res.add(clazz);

            // First, add all the interfaces implemented by this class
            Class<?>[] interfaces = clazz.getInterfaces();
            if (interfaces.length > 0) {
                res.addAll(Arrays.asList(interfaces));

                for (Class<?> interfaze : interfaces) {
                    res.addAll(getAllInterfaces(interfaze));
                }
            }

            // Add the super class
            Class<?> superClass = clazz.getSuperclass();

            // Interfaces does not have java,lang.Object as superclass, 
            // they have null, so break the cycle and return
            if (superClass == null) {
                break;
            }

            // Now inspect the superclass 
            clazz = superClass;
        } while (!"java.lang.Object".equals(clazz.getCanonicalName()));

        return new HashSet<Class<?>>(res);
    }

    private static Set<Class<?>> getAllInterfaces(Class<?> clazz)
    {
        List<Class<?>> res = new ArrayList<>();
        Stack<Class<?>> st = new Stack<>();
        st.push(clazz);
        while(! st.empty())
        {
            Class<?> current = st.pop();
            if ("java.lang.Object".equals(current.getCanonicalName())) {
                continue;
            }

            // find out and push to stack all interfaces of current class/interface
            Class<?>[] interfaces = current.getInterfaces();
            for (Class<?> intfc : interfaces) {
                res.add(intfc);
                st.push(intfc);
            }

            // find out the super class
            Class<?> superClass = current.getSuperclass();
            
            // interfaces does not have java,lang.Object as superclass, 
            // they have null, so break the cycle and return
            if (superClass == null) {
                continue;
            }
            
            // Now inspect the superclass 
            st.push(superClass);
        }
        return new HashSet<Class<?>>(res);
    }

    protected void getPlugins()
    {
        File dir = new File(System.getProperty("user.dir") + File.separator + pluginsDir);
        ClassLoader cl = new PluginClassLoader(dir);
        if (dir.exists() && dir.isDirectory())
        {
            // we'll only load classes directly in this directory -
            // no subdirectories, and no classes in packages are recognized
            String[] files = dir.list();
            for (int i=0; i < files.length; i++)
            {
                try {
                    // only consider files ending in ".class"
                    if (! files[i].endsWith(".class")) {
                        continue;
                    }

                    Class c = cl.loadClass(files[i].substring(0, files[i].indexOf(".")));

                    Set<Class<?>> classes = getAllInterfaces(c);
                    
                    // Class[] intf = c.getInterfaces();
                    // for (int j=0; j<intf.length; j++)
                    for (Class clazz: classes)
                    {
                        if (/*intf[j]*/clazz.getSimpleName().equals("Plugin")) {
                            // the following line assumes that Plugin has a no-argument constructor
                            Plugin pf = (Plugin) c.newInstance();
                            plugins.add(pf);
                            continue;
                        }
                    }
                }
                catch (Exception ex) {
                    System.err.println("File " + files[i] + " does not contain a valid Plugin class.");
                }
            }
        }
    }

    protected void runPlugins()
    {
        int count = 1;
        Iterator iter = plugins.iterator();
        while (iter.hasNext())
        {
            Plugin pf = (Plugin) iter.next();
            try {
                // pf.setParameter(count);
                // System.out.print(pf.getPluginName());
                // System.out.print(" ( "+count+" ) = ");
                // if (pf.hasError()) {
                //     System.out.println("there was an error during plugin initialization");
                //     continue;
                // }
                // int result = pf.getResult();
                // if (pf.hasError()) {
                //     System.out.println("there was an error during plugin execution");
                // }
                // else {
                //     System.out.println(result);
                // }
                
                ProcessContext ctx = new ProcessContext();
                ctx.setElements(elements);
                pf.process(ctx);

                count++;
            }
            catch (SecurityException secEx) {
                System.err.println("plugin '"+pf.getClass().getName()+"' tried to do something illegal");
            }
        }
    }
}



/**
* In order to impose tight security restrictions on untrusted classes but
* not on trusted system classes, we have to be able to distinguish between
* those types of classes. This is done by keeping track of how the classes
* are loaded into the system. By definition, any class that the interpreter
* loads directly from the CLASSPATH is trusted. This means that we can't
* load untrusted code in that way--we can't load it with Class.forName().
* Instead, we create a ClassLoader subclass to load the untrusted code.
* This one loads classes from a specified directory (which should not
* be part of the CLASSPATH).
*/
class PluginClassLoader extends ClassLoader {
    /** This is the directory from which the classes will be loaded */
    File directory;

    /** The constructor. Just initialize the directory */
    public PluginClassLoader (File dir) {
        directory = dir;
    }

    /** A convenience method that calls the 2-argument form of this method */
    public Class loadClass (String name) throws ClassNotFoundException { 
      return loadClass(name, true); 
    }

    /**
     * This is one abstract method of ClassLoader that all subclasses must
     * define. Its job is to load an array of bytes from somewhere and to
     * pass them to defineClass(). If the resolve argument is true, it must
     * also call resolveClass(), which will do things like verify the presence
     * of the superclass. Because of this second step, this method may be called to
     * load superclasses that are system classes, and it must take this into account.
     */
    public Class loadClass (String classname, boolean resolve) throws ClassNotFoundException
    {
      try {
        // Our ClassLoader superclass has a built-in cache of classes it has
        // already loaded. So, first check the cache.
        Class c = findLoadedClass(classname);

        // After this method loads a class, it will be called again to
        // load the superclasses. Since these may be system classes, we've
        // got to be able to load those too. So try to load the class as
        // a system class (i.e. from the CLASSPATH) and ignore any errors
        if (c == null) {
          try { c = findSystemClass(classname); }
          catch (Exception ex) {}
        }

        // If the class wasn't found by either of the above attempts, then
        // try to load it from a file in (or beneath) the directory
        // specified when this ClassLoader object was created. Form the
        // filename by replacing all dots in the class name with
        // (platform-independent) file separators and by adding the ".class" extension.
        if (c == null) {
          // Figure out the filename
          String filename = classname.replace('.',File.separatorChar)+".class";

          // Create a File object. Interpret the filename relative to the
          // directory specified for this ClassLoader.
          File f = new File(directory, filename);

          // Get the length of the class file, allocate an array of bytes for
          // it, and read it in all at once.
          int length = (int) f.length();
          byte[] classbytes  = new byte[length];
          DataInputStream in = new DataInputStream(new FileInputStream(f));
          in.readFully(classbytes);
          in.close();

          // Now call an inherited method to convert those bytes into a Class
          c = defineClass(classname, classbytes, 0, length);
        }

        // If the resolve argument is true, call the inherited resolveClass method.
        if (resolve) resolveClass(c);

        // And we're done. Return the Class object we've loaded.
        return c;
      }
      // If anything goes wrong, throw a ClassNotFoundException error
      catch (Exception ex) { throw new ClassNotFoundException(ex.toString()); }
    }
}


/**
* This is a fairly uptight security manager subclass. Classes loaded by
* the PluginClassLoader are highly restricted in what they are allowed to do.
* This is okay, because they're only supposed to calculate some values,
* for which all necessary data is already available to them.
*
* A SecurityManager consists of various methods that the system calls to
* check whether certain sensitive operations should be allowed. These
* methods can throw a SecurityException to prevent the operation from
* happening. With this SecurityManager, we want to prevent untrusted
* code that was loaded by a class loader from performing those sensitive operations.
* So we use inherited SecurityManager methods to check whether the call is being
* made by an untrusted class. If it is, we throw an exception.
* Otherwise, we simply return, allowing the operation to proceed normally.
*/
class PluginSecurityManager extends SecurityManager {

    private String pluginDir = null;

    public PluginSecurityManager (String dir) {
        pluginDir = dir;
    }

    /**
     * This is the basic method that tests whether there is a class loaded
     * by a ClassLoader anywhere on the stack. If so, it means that that
     * untrusted code is trying to perform some kind of sensitive operation.
     * We prevent it from performing that operation by throwing an exception.
     * trusted() is called by most of the check...() methods below.
     */
    protected void trusted() {
        if (inClassLoader()) throw new SecurityException();
    }

    /* added by javatarix */
    public void checkPermission(java.security.Permission perm) { trusted(); }
    public void checkPermission(java.security.Permission perm, Object context) { trusted(); }

    /**
     * These are all the specific checks that a security manager can
     * perform. They all just call one of the methods above and throw a
     * SecurityException if the operation is not allowed. This 
     * SecurityManager subclass is perhaps a little too restrictive. For
     * example, it doesn't allow loaded code to read *any* system properties,
     * even though some of them are quite harmless.
     */
    public void checkCreateClassLoader() { trusted(); }   
    public void checkAccess (Thread g) { trusted(); }
    public void checkAccess (ThreadGroup g) { trusted(); }
    public void checkExit (int status) { trusted(); }
    public void checkExec (String cmd) { trusted(); }
    public void checkLink (String lib) { trusted(); }
    public void checkRead (java.io.FileDescriptor fd) { trusted(); }
    public void checkRead (String file) {
//      String path = new File(file).getParentFile().getAbsolutePath();
//      if (! path.endsWith(pluginDir))
            trusted();
    }
    public void checkRead (String file, Object context) { trusted(); }
    public void checkWrite (java.io.FileDescriptor fd) { trusted(); }
    public void checkWrite (String file) { trusted(); }
    public void checkDelete (String file) { trusted(); }
    public void checkConnect (String host, int port) { trusted(); }
    public void checkConnect (String host,int port,Object context) {trusted();}
    public void checkListen (int port) { trusted(); }
    public void checkAccept (String host, int port) { trusted(); }
    public void checkMulticast (java.net.InetAddress maddr) { trusted(); }
    public void checkMulticast (java.net.InetAddress maddr, byte ttl) { trusted(); }
    public void checkPropertiesAccess() { trusted(); }
    public void checkPropertyAccess (String key) {
//      if (! key.equals("user.dir"))
            trusted();
    }
    public void checkPrintJobAccess() { trusted(); }
    public void checkSystemClipboardAccess() { trusted(); }
    public void checkAwtEventQueueAccess() { trusted(); }
    public void checkSetFactory() { trusted(); }
    public void checkMemberAccess (Class clazz, int which) { trusted(); }
    public void checkSecurityAccess (String provider) { trusted(); }

    /** Loaded code can only load classes from java.* packages */
    public void checkPackageAccess (String pkg) { 
        if (inClassLoader() && !pkg.startsWith("java.") && !pkg.startsWith("javax."))
            throw new SecurityException();
    }

    /** Loaded code can't define classes in java.* or sun.* packages */
    public void checkPackageDefinition (String pkg) { 
        if (inClassLoader() && ((pkg.startsWith("java.") || pkg.startsWith("javax.") || pkg.startsWith("sun."))))
            throw new SecurityException();
    }

    /** 
     * This is the one SecurityManager method that is different from the
     * others. It indicates whether a top-level window should display an
     * "untrusted" warning. The window is always allowed to be created, so
     * this method is not normally meant to throw an exception. It should
     * return true if the window does not need to display the warning, and
     * false if it does. In this example, however, our text-based Service
     * classes should never need to create windows, so we will actually
     * throw an exception to prevent any windows from being opened.
     **/
    public boolean checkTopLevelWindow (Object window) { 
        trusted();
        return true; 
    }
}