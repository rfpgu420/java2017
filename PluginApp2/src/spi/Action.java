package spi;

public enum Action {

    INIT     (PreProcessable.class),
    EXECUTE  (Processable.class),
    FINALIZE (PostProcessable.class);
 
    private final Class<? extends Plugin> plugin;

    private Action(Class<? extends Plugin> plugin) {
        this.plugin = plugin;
    }

    public Class<? extends Plugin> getInterface() {
        return plugin;
    }
}