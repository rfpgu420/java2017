package spi;

public class ProcessContext {
    
    private  int[][] elements;

    public ProcessContext(){
    }

    public void setElements(int[][] elements) {
        this.elements = elements;
    }

    public int[][] getElements() {
        return elements;
    }
}