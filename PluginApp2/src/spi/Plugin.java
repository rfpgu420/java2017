package spi;

import java.util.Map;
import java.util.HashMap;

public interface Plugin {
    public void process(ProcessContext ctx);
}