class Person {
    
    protected String lastName;
    protected String firstName;
    protected String middleName;

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public void setFirstName(String fName) {
        firstName = fName;
    }
    public void setMiddleName(String mName) {
        middleName = mName;
    }

    public void setFullName(
        String lName,
        String fName,
        String mName
    ) {
        setLastName(lName);
        setFirstName(fName);
        setMiddleName(mName);
    }
}

class Employee extends Person {

    private String empNumber;

    private String lastName;

    public void setEmpNumber(String empNumber) {
        this.empNumber = empNumber;
    }
}

class Manager extends Employee {

    protected Department  dept;

    public void setDepartment(Department d) {
        dept = d;
    }

    Manager() {
        // lastName   = "";
        firstName  = "";
        middleName = "";
    }
}

class Department {

}


class Organization {

    private Employee[]   emps;
    private Department[] depts;
    private Manager      mgr;

    public void setManger(Manager mgr) {
        this.mgr = mgr;
    }     
}

public class Composition {

    public static void main(String[] args) {
        System.out.println("start app");
        // Employee e = new Employee();
        Manager m = new Manager();
        
    }

}