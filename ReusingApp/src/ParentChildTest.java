class Parent {

    private int x = 0;

    public Parent() {
        f2(); // DANGEROUS !!!
        // doAction();
    }

    private void doAction() {
        System.out.println("Parent.doAction(), x = " + x);    
    }

    public void f1() {
        f2();
    }

    protected void f2() {
        System.out.println("Parent.f2(), x = " + x);
    }
}

class Child extends Parent {

    private int x;

    public Child() {
        x = 1;
    }
    
    @Override
    public void f2() {
        System.out.println("Child.f2(), x = " +x);
    }
}

public class ParentChildTest {

    public static void main(String[] args) {
        Parent p = new Child();
        // p.f1();
    }    
}