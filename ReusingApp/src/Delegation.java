
public class Delegation {

    public static void main(String[] args) {
        UserActions actions = new UserActions();

        actions.buttonHaltAction();
        actions.buttonRebootAction();
    }
}

class UserActions {

    //private PKController pc_controller = new PKController();
    // private PKController pc_controller = null;
    private PKControll pc_controller = null;

    public void setPKController(PKControll pkCtrl) {
        pc_controller = pkCtrl;
    }

    public void buttonHaltAction() {
        pc_controller.halt();
    }

    public void buttonRebootAction() {
        pc_controller.reboot();
    }
}

interface  PKControll {

    public void halt();
    public void reboot();
}


class PKController implements PKControll {
    
    public void halt() {
        System.out.println("Halting...");
    }

    public void reboot() {
        System.out.println("Rebooting...");
    }
}

