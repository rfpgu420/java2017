public class FinalUsage {
    
    public final int z;
    
    public final FinalUsage s = new FinalUsage();

    public final int x = 7;
    
    public static final int TRUE_CONSTANT = 7;


    public void f1() {
        final int x = 8;

        s.f2(7);
    }

    public void f2(final int x) {
        // x = 10;
        System.out.println("f2()");
    }

    public final void f3() {
    }

    public static final void f4() {
    }

    class TryFinalOverride extends FinalUsage {

        // @Override public void f3() {
        //     System.out.println("TryFinalOverride.f3()");
        // }
    }

    final class InternalClass {

    }

    FinalUsage() {
        z = 1;
    }

    public static void main(String[] args) {
        
    }

}