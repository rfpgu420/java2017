class Person {
    private String lName;
    private String fName;
    private String mName;
}

class Candidate extends Person {
    public Candidate() {
        System.out.println("Candidate()");
    }
}

class Employee extends Person {
    public Employee() {
        System.out.println("Employee()");
    }
}

class Manager extends Employee {
    public Manager() {
        System.out.println("Manager()");
    }

    public void hireEmployee() {
        System.out.println("Manager.hireEmployee()");
    }
}

class Superviser extends Manager {
    public Superviser() {
        System.out.println("Superviser()");
    }    
}


class ManagerService {
    
    public Employee getEmployeeWithMaxSalary() {
        return new Manager();
    }

    // public int getSubordinateCount() {
    //     return 5;
    // }
}

class SuperviserService extends ManagerService {
    
    @Override
    public Superviser getEmployeeWithMaxSalary() {
        return new Superviser();
    }

    // @Override
    // public long getSubordinateCount() {
    //     return (short)3;
    // }
}


public class CovariantReturnTest {
    
    public static void main(String[] args) {
        ManagerService svService = new SuperviserService();
        svService.getEmployeeWithMaxSalary();

        Employee[] emps = new Employee[] {
            new Manager(), new Manager(), new Employee()
        };

        for (Employee e : emps) {
            ((Manager)e).hireEmployee();
        }
    }
}