class Cleanser {
    protected String s = "Cleanser";

    public void append(String s) { this.s += s; }
    public void dilute() { append(" dilute()"); }
    public void apply()  { append(" apply()"); }
    public void scrub()  { append(" scrub()"); }

    public String toString() { return s; }

    public static void main(String[] args) {
        Cleanser x = new Cleanser();
        x.dilute();
        x.apply();
        x.scrub();
        System.out.println(x);
    }
}

public class Detergent extends Cleanser {

    // private String s = "Detergent";

    // public void append() {}
    
    public void append(String s) { this.s += s; }

    public void scrub() {
        append(" Detergent.scrub()");
        super.scrub();
    }

    public void foam() { append(" foam()"); }

    // public String toString() {
    //     return s;
    // }

    public static void main(String[] args) {
        Detergent x = new Detergent();
        x.dilute();
        x.apply();
        x.scrub();
        x.foam();
        System.out.println(x);
        System.out.println("Checking base class");
        Cleanser.main(args);
    }
}