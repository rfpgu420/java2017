interface Disposable {
    public void dispose();
}


class Garbage implements Disposable {
    public void destroy() {
        System.out.println("Garbage.destroy()");
    }

    @Override public void dispose() {
        System.out.println("Garbage.dispose()");
    }

    @Override public void finalize() {
        System.out.println("Garbage.finalize()");
    }
}

class Glass extends Garbage {
    private boolean disposed = false;

    @Override public void destroy() {
        System.out.println("Glass.destroy()");
    }

    @Override public void dispose() {
        System.out.println("Glass.dispose()");
        disposed = true;

        super.dispose();
    }

    @Override public void finalize() {
        if (! disposed) {
            System.out.println("ERROR! Glass object can't finalize safety");
        }
        super.finalize();
    }
}

class Plastic extends Garbage {
    private boolean disposed = false;

    @Override public void destroy() {
        System.out.println("Plastic.destroy()");
    }

    @Override public void dispose() {
        System.out.println("Plastic.dispose()");
        disposed = true;

        super.dispose();
    }

    @Override public void finalize() {
        if (! disposed) {
            System.out.println("ERROR! Plastic object can't finalize safety");
        }
        super.finalize();
    }
}

class Paper extends Garbage {
    private boolean disposed = false;

    @Override public void destroy() {
        System.out.println("Paper.destroy()");
    }

    @Override public void dispose() {
        System.out.println("Paper.dispose()");
        disposed = true;

        super.dispose();
    }

    @Override public void finalize() {
        if (! disposed) {
            System.out.println("ERROR! Paper object can't finalize safety");
        }
        super.finalize();
    }
}

class HardPaper extends Paper {
    private boolean disposed = false;

    @Override public void destroy() {
        System.out.println("HardPaper.destroy()");
    }

    @Override public void dispose() {
        System.out.println("HardPaper.dispose()");
        disposed = true;

        super.dispose();
    }

    @Override public void finalize() {
        if (! disposed) {
            System.out.println("ERROR! HardPaper object can't finalize safety");
        }
        super.finalize();
    }
}

class GarbageDestroyMachine {
    private Garbage[] g;

    public void collect(Garbage[] g) {
        this.g = g;
    }

    public void destroy() {
        for (Garbage gi : g)
        {
            try {
                gi.destroy();
            }
            finally {
                gi.dispose();
            }
        }
    }
}

public class DisposeIt {
    
    public static void main(String[] args) {
        GarbageDestroyMachine gdm = new GarbageDestroyMachine();
        // gdm.collect(
        //     new Garbage[] {
        //         new Glass(), new Glass(),
        //         new Paper(), new Plastic(),
        //         new HardPaper()
        //     }
        // );
        // gdm.destroy();

        gdm.collect(
            new Garbage[] {
                new Glass(), new Glass(),
                new Paper(), new Plastic(),
                new HardPaper()
            }
        );

        gdm.collect(
            new Garbage[] {
                new Glass(), new Glass(),
                new Paper(), new Plastic(),
                new HardPaper()
            }
        );

        gdm.collect(
            new Garbage[] {
                new Glass(), new Glass(),
                new Paper(), new Plastic(),
                new HardPaper()
            }
        );

        System.gc();
    }
}