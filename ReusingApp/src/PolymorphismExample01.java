class Shape {
    
    protected void draw() {
        System.out.println("Shape.draw()");
    }

    public void destroy() {
        System.out.println("Shape.destroy()");
    }
}

class Circle extends Shape {

    public void draw() {
        System.out.println("Circle.draw()");
    }

    public void destroy() {
        System.out.println("Circle.destroy()");
    }
}

class Triangle extends Shape {
    
    public void destroy() {
        System.out.println("Triangle.destroy()");
    }
}

class Square extends Shape {

    public void destroy() {
        System.out.println("Square.destroy()");
    }
}

class Elipse extends Shape {

}


class Canvas {

    // private Triangle[] t;
    // private Circle[]   c;
    // private Square[]   s;
    // private Elipse[]   e;

    private Shape[] shapes;
}


public class PolymorphismExample01 {
    
    public static void main(String[] args) {
        Shape[] shapes = new Shape[]{
            new Triangle(), 
            new Square(),
            new Circle()
        };

        for (Shape shape : shapes) {
            shape.draw();
            shape.destroy();
        }
    }
}