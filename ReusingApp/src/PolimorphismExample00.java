/**
 * @see  http://blog.jamesdbloom.com/JVMInternals.html
 * @see  http://www.artima.com/insidejvm/ed2/jvm2.html
 * @see  https://www.acloudtree.com/hacking-java-bytecode-for-programmers-part1-the-birds-and-the-bees-of-hex-editing/
 */
class First {

    public First() {
        f1(); // DANGEROUS !!!
    }
    
    public void f1() {
        f2();
    }

    public void f2() {
        System.out.println("First.f2()");
    }
}

class Second extends First{
    
    public void f2() {
        System.out.println("Second.f2()");
    }    
}

public class PolimorphismExample00 {
    
    public static void main(String[] args) {
        First obj = new Second();
        // obj.f1();
    }
}