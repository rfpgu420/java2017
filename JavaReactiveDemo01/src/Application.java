// package org.mytutor;

// java standard libraries
import java.util.*;
import java.io.*;
import java.nio.*;
import java.net.*;

import java.util.concurrent.*;

// rxJava2
import io.reactivex.*;
import io.reactivex.functions.*;
import io.reactivex.schedulers.*;


import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

// Jsoup
import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

// application packages

/**
 * Main applacition class.
 */
public class Application {
 
    static class Quizz {

        private List<Question>   questions;

        public Quizz() {
            questions = new ArrayList<>();
        }

        public Quizz(List<Question> questions) {
            this.questions = questions;
        }

        public void addQuestion(Question question) {
            questions.add(question);
        }

        public void removeQuestion(Question question) {
            questions.remove(question);
        }

        public String toString() {
            StringBuffer questionText = new StringBuffer();
            for (Question q : questions) {
                questionText.append(q);
                questionText.append("\n");
            }
            return questionText.toString();
        }
    }
    
    static class Question {
        private int          id = -1;
        private String       text;
        private List<Answer> answers = new ArrayList<Answer>();

        private Question() {// private constructor
        }

        public int getId() {
            return id;
        }

        public String getText() {
            return text;
        }

        public Answer getAnswer(int index) {
            return answers.get(index);
        }

        public List<Answer> getAnswers() {
            return answers;
        }

        public void addAnswer(Answer answer) {
            answers.add(answer);
        }

        public void addAnswer(int id, String text, boolean isCorrect) {
            answers.add(Answer.newBuilder()
                .setId(id)
                .setText(text)
                .setCorrect(isCorrect)
                .build()
            );
        }

        @Override
        public String toString() {
            StringBuffer answersText = new StringBuffer();
            answersText.append(text);
            answersText.append("\n");
            for (Answer a : answers) {
                answersText.append(a);
                answersText.append("\n");
            }
            return answersText.toString();
        }

        public static Builder newBuilder(Question question) {
            return question.new Builder();
        }

        public static Builder newBuilder() {
            return new Question().new Builder();
        }

        public class Builder {

            private Builder() {// private constructor
            }

            public Builder setId(int id) {
                Question.this.id = id;
                return this;
            }

            public Builder setText(String text) {
                Question.this.text = text;
                return this;
            }

            public Builder setAnswers(List<Answer> answers) {
                Question.this.answers = answers;
                return this;
            }

            public Question build() {
                return Question.this;
            }
        }
    }

    static class Answer {
        private int     id = -1;
        private String  text;
        private boolean isCorrect;

        private Answer() {// private constructor
        }

        public int     getId()     { return id;}
        public String  getText()   { return text; }
        public boolean isCorrect() { return isCorrect; }
        
        @Override
        public String  toString()  {
            StringBuffer sb = new StringBuffer();
            sb.append((isCorrect ? "[x]" : "[ ]"));
            sb.append(id);
            sb.append(". ");
            sb.append(text);
            return sb.toString();
        }

        public static Builder newBuilder(Answer answer) {
            return answer.new Builder();
        }

        public static Builder newBuilder() {
            return new Answer().new Builder();
        }

        public class Builder {

            private Builder() {
                // private constructor
            }

            public Builder setId(int id) {
                Answer.this.id = id;
                return this;
            }

            public Builder setText(String text) {
                Answer.this.text = text;
                return this;
            }

            public Builder setCorrect(boolean isCorrect) {
                Answer.this.isCorrect = isCorrect;
                return this;
            }

            public Answer build() {
                return Answer.this;
            }
        }
    }

    private static List<Question> questions;


    private static String[] urls = {
        "http://upworktest.in/upwork-java-technologies/java-test-v3-2016/",
        "http://upworktest.in/upwork-net-technology/c-test-2016/",
        "http://upworktest.in/upwork-odesk-%e2%80%a2-databases/oracle-pl-sql-10g-test-2016/",
    };

    // public List<Question> parse(String url)
    // {        
    //     try {
    //         done = false;

    //         Document doc     = Jsoup.connect(url).header("charset","UTF-8").get();
    //         Element  content = doc.select("div.entry-content").first();
    //         Elements entries = content.children();        
            
    //         Question     question      = new Question();
    //         StringBuffer questionText  = new StringBuffer();
    //         boolean      isQuestionNew = false;
    //         for (Element entry : entries)
    //         {
    //             question = isQuestionNew ? new Question() : question;

    //             if (entry.tagName().equals("p"))
    //             {
    //                 // accumulate question text in buffer
    //                 questionText.append(entry.text());
    //                 questionText.append("\n");
    //                 isQuestionNew = false;
    //             }
    //             else if (entry.tagName().equals("ol"))
    //             {
    //                 // set question text
    //                 question.setText(questionText.toString());
    //                 // clean question text buffer
    //                 questionText.delete(0, questionText.length());
    //                 // parse answers
    //                 for (Element item : entry.children()) {
    //                     String  answerText = item.text();    
    //                     boolean isCorrect  = item.select("span").first() != null;
    //                     question.addAnswer(answerText, isCorrect);
    //                 }
    //                 // fill question list
    //                 questionList.add(question);
    //                 // prepare for new question parsing
    //                 isQuestionNew = true;

    //                 // notify observers new question was parsed
    //                 setChanged();
    //                 notifyObservers(question);
    //             }
    //         }
    //     }
    //     catch (IOException ioe) {
    //     }        
    //     finally {
    //         done = true;
    //     }

    //     return prepareResult();
    // }


    public static void main(String[] args)
    {
        final Quizz  quizz = new Quizz();

        final String url = urls[0];

        Observable.fromCallable(new Callable<Elements>() {
            @Override
            public Elements call() throws Exception {
                return Jsoup.connect(url)
                            .header("charset","utf-8")
                            .timeout(0)
                            .get()
                            .select("div.post-content")
                            .first()
                            .children();
            }
        })
        .flatMap(new Function<List<Element>, ObservableSource<Element>>() {
            @Override
            public ObservableSource<Element> apply(List<Element> elements) {
                return Observable.fromIterable(elements);
            }
        })
        .doOnNext(new Consumer<Element>() {
            private StringBuffer sb = new StringBuffer();

            @Override
            public void accept(Element element) {
                if (element.tagName().equals("p"))
                {
                    // accumulate question text in buffer
                    sb.append(element.text());
                    sb.append("\n");
                }
                else if (element.tagName().equals("ol"))
                {
                    Question question = Question.newBuilder()
                                                .setText(sb.toString())
                                                .build();
                    sb.delete(0, sb.length());

                    // parse answers
                    for (Element item : element.children()) {
                        String  answerText = item.text();    
                        boolean isCorrect  = item.select("span").first() != null;
                        question.addAnswer(0, answerText, isCorrect);
                    }

                    // fill question list
                    quizz.addQuestion(question);
                }
            }
        })
        // .filter(new Predicate<Element>() {
        //     @Override
        //     public boolean test(Element element) {
        //         return element.tagName().equals("ol");
        //     }
        // })
        .subscribe(new Consumer<Element>() {
            @Override 
            public void accept(Element element) {
                //System.out.println(element);
            }
        });
        
        System.out.println(quizz);
                
        // Observable<Element> observable = Observable.create(
        //     new ObservableOnSubscribe<Element>() {
        //         @Override         
        //         public void subscribe(ObservableEmitter<Element> e) throws Exception {
        //             Document doc      = Jsoup.connect(url).timeout(0).header("charset","utf-8").get();
        //             Element  content  = doc.select("div.post-content").first();
        //             Elements elements = content.children();

        //             // emit each item in the stream
        //             for (Element element : elements) {
        //                 e.onNext(element);
        //             }

        //             // Observable has emitted all items of sequence
        //             e.onComplete();
        //         }
        //     }
        // );
        // 
        // Observer<Element> observer = new Observer<Element>() {
        //     private Question     question      = new Question();
        //     private StringBuffer questionText  = new StringBuffer();

        //     @Override
        //     public void onSubscribe(Disposable d) {
        //         // Log.e(TAG, "onSubscribe: ");
        //     }

        //     @Override
        //     public void onNext(Element element) {
        //         System.out.println(element.text());
        //     }

        //     @Override
        //     public void onError(Throwable e) {
        //         // Log.e(TAG, "onError: ");
        //         System.out.println(e);                
        //     }

        //     @Override
        //     public void onComplete() {
        //         // Log.e(TAG, "onComplete: All Done!");
        //     }
        // };
         
        // // create our subscription
        // observable.subscribe(observer);
    }
}