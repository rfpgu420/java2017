@echo off
rem echo "create directory for plugin's classes"
rem mkdir .\bin\plugins
rem echo "copy res/input.txt into bin/plugins/input.txt"
rem copy /Y .\res\input.txt .\bin\plugins\input.txt
rem echo "compile app sources"
rem dir /s /B src\plugins\*.java > sources.txt
rem javac -Xlint:deprecation -Xlint:unchecked -cp ".;src/;lib/plugin-demo3.jar" -g -d bin/plugins @sources.txt
rem echo "run app using explicit entry point"
rem java -cp ".;lib/plugin-demo3.jar" org.plugindemo.PluginDemo bin\plugins
rem pause
rem echo "run app using default entry point"
java -jar dist/rxjava-demo.jar %*