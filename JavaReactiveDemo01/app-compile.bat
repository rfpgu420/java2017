@echo off
set "home=."
set "dist=%home%/dist"
set "libs=%home%/src/;%home%/lib/rxjava/*;%home%/lib/net/*;%home%/lib/json/*;%home%/lib/jdbc/sqlite/*"
set "cp=%home%;%libs%"
@echo on
javac -Xlint:deprecation -Xlint:unchecked -cp "%cp%" -g -d "%dist%" src/Bootstrap.java
javac -Xlint:deprecation -Xlint:unchecked -cp "%cp%" -g -d "%dist%" src/Application.java
@pause