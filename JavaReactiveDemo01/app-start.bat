@echo off
set "home=."
set "dist=%home%/dist/"
set "libs=%home%/lib/rxjava/*;%home%/lib/net/*;%home%/lib/json/*;%home%/lib/jdbc/sqlite/*"
set "cp=%home%;%dist%;%libs%"
@echo on
java -cp "%cp%" Bootstrap %*