rem compile plugin-demo3.jar sources
dir /s /B src\*.java > tmp\sources.txt
javac -Xlint:deprecation -Xlint:unchecked -cp ".;src/;lib/rxjava/*;lib/net/*;lib/json/*;lib/jdbc/sqlite/*;" -g -d bin/ @tmp\sources.txt
rem make plugin-demo3.jar
jar cfvm dist\rxjava-demo.jar manifest.mf -C bin . lib\rxjava lib\net\jsoup-1.9.2.jar lib\json\json.jar lib\jdbc\sqlite\sqlite-jdbc-3.16.1.jar