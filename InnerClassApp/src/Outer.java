public class Outer {
    
    class Inner {

    }

    public Inner makeInner() {
        return new Inner();
    }

    public static void main(String[] args) {
        Outer.Inner inner  = new Outer().makeInner(); 
        Outer.Inner inner2 = new Outer().new Inner(); 
    }
}