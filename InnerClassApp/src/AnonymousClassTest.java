class BaseA {
    private int x;

    public BaseA(){
        x = 10;
    }

    public BaseA(int x) {
        this.x = x;
    }

    public int getInt() {
        return x;
    }
}

abstract class BaseB {
    abstract void printMe();
}

interface BaseC {
    void printMe();
}


class Context {
    private int i;
    
    public void setInt(int i) {
        this.i = i;
    }
    public int getInt() {
        return i;
    }
}


public class AnonymousClassTest {
    
    private class AA extends BaseA {
        private int i = 47;
        public int getInt() {
            return this.i;
        }
    }

    public BaseA usualGetA() {
        return new AA();
    }

    private int outerInt = 10;

    private int getOuterInt() {
        return outerInt;
    }

    public BaseA getA(int x) {
        return new BaseA(x) {
            private int i;

            {
                i = 47;
            }

            // public void init(Context ctx) {
            //     i = ctx.getInt();
            // }

            // public void setInt(int i) {
            //     this.i = i;
            // }

            public int getInt() {
                // System.out.println("outerInt(): "+outerInt);
                return super.getInt() * i;
            }
        };
    }

    public BaseB getB() {
        return new BaseB() {
            public void printMe() {
                System.out.println("className = "+getClass().getSimpleName());
            }
        };
    }

    public BaseC getC() {
        return new BaseC() {
            public void printMe() {
                System.out.println("className = "+getClass().getSimpleName());
            }
        };
    }

    public static void main(String[] args) {
        AnonymousClassTest ac = new AnonymousClassTest();
        
        // BaseA ua = ac.usualGetA();
        // System.out.println("ua.getInt(): "+ua.getInt());

        BaseA a = ac.getA(5);
        System.out.println("a.getInt(): "+a.getInt());

        // BaseB b = ac.getB();
        // b.printMe();

        // BaseC c = ac.getC();
        // c.printMe();
    }
}