interface Printable {
    void printMe();
}

public class PrintTest {
    
    public Printable f() {
        {
            class Print implements Printable {
                public void printMe() {
                    System.out.println("PrintTest.f().Print.printMe()");
                }
            }
        }
        return new Print();
    }

    public static void main(String[] args) {
        PrintTest pt = new PrintTest();
        Printable p = pt.f();
        p.printMe();
    }
}