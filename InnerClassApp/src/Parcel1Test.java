public class Parcel1Test {
    

    public static void main(String[] args) {
        Parcel1 p1 = new Parcel1();
        Parcel1 p2 = new Parcel1();
        Parcel1 p3 = new Parcel1();

        p1.setX(1);
        Parcel1.Contents p1c1 = p1.new Contents();
        Parcel1.Contents p1c2 = p1.new Contents();
        Parcel1.Contents p1c3 = p1.new Contents();
        p1c1.printOuterX();
        p1c2.printOuterX();
        p1c3.printOuterX();

        p2.setX(2);
        Parcel1.Contents p2c1 = p2.new Contents();
        p2c1.printOuterX();

        p3.setX(3);
        Parcel1.Contents p3c1 = p3.new Contents();
        p3c1.printOuterX();
    }
}