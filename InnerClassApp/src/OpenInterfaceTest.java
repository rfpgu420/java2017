interface OpenInterface {
    
}

public class OpenInterfaceTest {
    
    private class P implements OpenInterface {
        public void t(){
            System.out.println("Hello World");
        }
    }

    public OpenInterface getP() {
        return new P();
    }

    public static void main(String[] args) {
        OpenInterfaceTest oit = new OpenInterfaceTest();

        OpenInterface oi = oit.getP();

        OpenInterfaceTest.P p = (OpenInterfaceTest.P)oi;
        // ((OpenInterfaceTest.P) oi).t();
    }
}