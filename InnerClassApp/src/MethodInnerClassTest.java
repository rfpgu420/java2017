public class MethodInnerClassTest {
    
    class A {}

    public void print() {
        class B {
            private String name = "TEST";
            public String getName() {
                return name;
            }
        }

        B b = new B();
        System.out.println(
            getClass().getSimpleName()+".print().B.getName(): "+b.getName()
        );
    }

    public void print(boolean b) {
        if (b) {
            class CC {
                private int x = 10;
                public int getX() {
                    return x;
                }
            }

            System.out.println(
                "MethodInnerClassTest.print(b).C.getX():"+(new CC().getX())
            );
        }

        // new CC();
    }

    public static void main(String[] args) {
        MethodInnerClassTest o = new MethodInnerClassTest();
        o.print();
    }
}