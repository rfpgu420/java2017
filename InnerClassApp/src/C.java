interface A {
    
}

interface B {
    
}


public class C {

    // nested class 
    static class S {

        public void t() {
            // getTestA(); //ERROR!!!
        }

        public static void f() {
            System.out.println("C.S.f()");
        }
    }
    // inner class
    class TestA implements A {
        // public static int x = 0;
        // public static void f() { }
    }
    // inner class
    class TestB implements B {
    }

    public A getTestA() {
        return new TestA();
    }
    public B getTestB() {
        return new TestB();
    }

    private void test() {}
    
    // inner class
    public class D {
        public void t() {
            getTestA();
            test();
        }
    }

    public static void main(String[] args) {
        // C c = new C();
        // C.S x = c.new S();

        C.S cs = new C.S(); // nested
    }
}