public class Parcel1 {

    private int x;

    public void setX(int x) {
        this.x = x;
    }

    public int getX() {
        return x;
    }

    class Contents {
        private int i = 11;

        public void printOuterX(){
            System.out.println(x);
        }

        /*
         * ERROR:  Illegal static declaration in inner class
         */
        // private static String s1 = "inner class static string";
        
        /*
         * ERROR:  Illegal static declaration in inner class
         */
        // public static String s2 = "inner class static string";
        
        private static final String prvtStaticFinalString = "inner class static string";
        public  static final String pblcStaticFinalString = "inner class static string";
        
        public int value() {
            return i;
        }

        /*
         * ERROR:  Illegal static declaration in inner class
         */
        // public static void main(String[] args) {
        //     System.out.println("Parsel.Contents.main()");
        // }
    }

    class Destination {
        private String label;

        Destination(String whereTo) {
            label = whereTo;
        }

        String readLabel() {
            return label;
        }

        private void prvtMethod() {
            System.out.println(getClass().getSimpleName()+".prvtMethod()");
        }

        /*
         * ERROR:  Illegal static declaration in inner class
         */
        // public static void main(String[] args) {
        //     System.out.println("Parcel1.Destination.main()");
        // }
    }

    public void ship(String dest) {
        Contents c = new Contents();
        Destination d = new Destination(dest);
        
        /*
         * ATTENTION:  access to private method of inner class.
         */
        d.prvtMethod();

        System.out.println(d.readLabel());
    }

    public static void main(String[] args) {
        Parcel1 p = new Parcel1();
        p.ship("America");
    }
}