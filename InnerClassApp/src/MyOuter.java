public class MyOuter {
 
    class MyInner {

        public void HelloMethod() {
            System.out.println("HelloMethod");
        }

        public MyOuter getOuter() {
            return new MyOuter();
        }

        public MyOuter getOuterInstance() {
            return MyOuter.this;
        }
    }

    public MyInner getInner() {
        return new MyInner();
    }
}