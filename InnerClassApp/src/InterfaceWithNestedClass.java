public interface InterfaceWithNestedClass {
 
    void f();

    class NestedClass {
        private int i;
        
        public NestedClass(int i) {
            this.i = i;
        }

        @Override
        public String toString() {
            return String.format("value of i is %1$d", i);
        }
    }
}