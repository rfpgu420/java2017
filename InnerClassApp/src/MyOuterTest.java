public class MyOuterTest {
    
    public static void main(String[] args) {
        MyOuter my_out = new MyOuter();
        MyOuter.MyInner  my_in = my_out.getInner();
        my_in.HelloMethod();

        MyOuter o = my_in.getOuter();
        o.getInner().HelloMethod();

        MyOuter o2 = my_in.getOuterInstance();

        if (my_out == o) {
            System.out.println("my_out = o");
        }

        if (my_out == o2) {
            System.out.println("my_out = o2");
        }

        MyOuter.MyInner newObject  = new MyOuter().new MyInner();
        MyOuter.MyInner newObject2 = my_out.new MyInner();
    }
}