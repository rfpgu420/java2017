abstract class Base {
    public Base(int i) {
        System.out.println("constructor of Base, i = " + i);
    }
    public abstract void f();
}

public class AnonymousConstructor {
    
    public static Base getBase(int i) {
        return new Base(i) {
            {
                System.out.println("instance initialization");
            }

            public void f() {
                System.out.println("in anonymous instatnce");
            }
        };
    }

    public static void main(String[] args) {
        Base base = getBase(47);
        base.f();
    }
}