import java.util.Date;
import java.util.Scanner;
import java.text.SimpleDateFormat;

public class EmpDeptRoutines {
    
    private Employee[]   employees;
    private Department[] departments;

    private void fillEmployees()
    {
        employees = new Employee[10];

        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy");

        String  input;
        Scanner scanner = new Scanner(System.in);
        while (true)
        {
            System.out.print("\nEnter new employee (y/n)?: ");
            input = scanner.nextLine();
            if ("n".equals(input)) {
                break;
            }

            Employee e = new Employee();
            try {
                System.out.print("Enter employee ID: ");
                e.setEmployeeId(Long.valueOf(scanner.nextLine()));

                System.out.print("Enter first name: ");
                e.setFirstName(scanner.nextLine());

                System.out.print("Enter last name: ");
                e.setLastName(scanner.nextLine());

                System.out.print("Enter email: ");
                e.setEmail(scanner.nextLine());

                System.out.print("Enter hire date (DD.MM.YYYY): ");
                e.setHireDate(df.parse(scanner.nextLine()));

                System.out.print("Enter phone number: ");
                e.setPhoneNumber(scanner.nextLine());

                System.out.print("Enter job id: ");
                e.setJobId(scanner.nextLine());

                System.out.print("Enter salary: ");
                e.setSalary(Double.valueOf(scanner.nextLine()));

                System.out.print("Enter commission percent: ");
                e.setCommissionPct(Double.valueOf(scanner.nextLine()));

                System.out.print("Enter manager ID: ");
                e.setCommissionPct(Long.valueOf(scanner.nextLine()));

                System.out.print("Enter department ID: ");
                e.setCommissionPct(Long.valueOf(scanner.nextLine()));
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }

            System.out.println("-----------\n");
            System.out.println("\nemployee: " + e);
            System.out.println("-----------\n");
        }
        scanner.close();
    }

    private void fillDepartments() {
        // read from console
        // save into Departments[]
    }

    // private Employee[] findEmployees(long deptId) {
    // }

    // private void printEmployees(Employee[] emps) {
    // }

    public static void main(String[] args) {        
        EmpDeptRoutines edr = new EmpDeptRoutines();
        edr.fillEmployees();        
        edr.fillDepartments();        
    }
}

class Employee {
    private long    id;
    private String  firstName;
    private String  lastName;
    private String  email;
    private String  phoneNumber;
    private Date    hireDate;
    private String  job_id;
    private double  salary;
    private double  commissionPct;
    private long    managerId;
    private long    departmentId;

    public void setEmployeeId(long id) {
        this.id = id;
    }
    public long getEmployeeId() {
        return id;
    }

    public void setHireDate(Date dt) {
        hireDate = dt;
    }
    public Date getHireDate() {
        return hireDate;
    }

    public void setFirstName(String firstName){}
    public void setLastName(String lastName){}
    public void setEmail(String email){}
    public void setPhoneNumber(String phoneNumber){}
    public void setJobId(String jobId){}
    public void setSalary(double salary){}
    public void setCommissionPct(double comm){}
    public void setManagerId(long managerId){}
    public void setDepartmentId(long deptId){}

    @Override
    public String toString() {
        return "id = "+id
        +      "\nfirstName = " + firstName; 
    }
}

class Department {
    private long   id;
    private String name;
    private long   managerId;
    private long   locationId;

    @Override
    public String toString() {
        return  "id = "+id
        +       "\nname = " + name
        +       "\nmanagerId = "+managerId
        +       "\nlocationId = "+locationId; 

        // String.format(
        //     "id = %1$s, name = %2$s, managerId = %3$d, locationId = %4$d"
        //     , id, name, managerId, locationId
        // );
    }
}