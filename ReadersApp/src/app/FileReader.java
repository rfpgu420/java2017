import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class FileReader {

    public static void main(String[] args) {
        FileReader obj = new FileReader();
        System.out.println(obj.printFile("res/input.txt"));

    }

    private String printFile(String fileName)
    {
        StringBuilder result = new StringBuilder("");

        //get file from resources folder
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());

        try (Scanner scanner = new Scanner(file))
        {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                result.append(line).append("\n");
            }
            scanner.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return result.toString();
    }
}