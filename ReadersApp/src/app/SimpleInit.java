public class SimpleInit /*extends Object*/{
    
    protected String  s;   // null

    private byte    b;    // 0
    private char    ch;   // ' '
    private short   sh;   // 0
    private int     i;    // 0
    private long    l;    // 0
    private float   f;    // 0.0
    private double  d;    // 0.0
    private boolean bb;   // false

    public static int t1;  // 0

    public static int t2;  // 0

    static {
        System.out.println("class init 1 start");
        System.out.println("t1 = " + t1);
        System.out.println("t2 = " + t2);
        System.out.println("class init 1 end");
    }


    {
        System.out.println("instance init 1 start");
        System.out.println(toString());
        System.out.println("instance init 1 end");
    }

    public SimpleInit() {   
        System.out.println("constructor SimpleInit()");
        s  = "Hi!";
        b  = (byte)1;
        ch = 'x';
        sh = (short)1;
        i  = 1;
        l  = 1L;
        f  = 1.0F;
        d  = 1.0D;
        bb = false;
    }
    
    {
        System.out.println("instance init 2 start");
        System.out.println(toString());
        System.out.println("instance init 2 end");
    }

    public SimpleInit(String s) {
        this.s = s;
    }

    @Override
    public String toString() {
        return  "string    = " + s
        +       "\nbyte    = " + b
        +       "\nchar    = ["+ch+"]"
        +       "\nshort   = " + sh
        +       "\nint     = " + i
        +       "\nlong    = " + l
        +       "\nfloat   = " + f
        +       "\ndouble  = " + d
        +       "\nboolean = " + bb;
    }

    public static void main(String[] args) {
        // System.out.println(new SimpleInit());
        // System.out.println(new SimpleInit("Hi everybody!!!"));
        
        for (int i=0; i < 1000000; i++)
            System.out.println(new NoSimpleInit());

        // System.gc();
    }

}

class NoSimpleInit extends SimpleInit {

    private boolean isSavedInDB  = false;

    public NoSimpleInit() {
        System.out.println("constructor NoSimpleInit()");
    }

    @Override
    public String toString() {
        return  "string    = " + s;
    }

    @Override
    public void finalize() throws  Exception{
        // finalise-ready condition here
        if (! isSavedInDB) {
            throw new Exception("ATENTION! Object is in use!!!");
        }
        
        System.out.println("NoSimpleInit.finalise()");
        try {
            super.finalize();
        }
        catch(Throwable ex) {
            ex.printStackTrace();
        }
    }

}