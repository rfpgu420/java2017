public class MyOverloading {
    
    private String s;
    private int    x;


    private void f() { 
        System.out.println("f()");       
    }

    private void f(int x, String s) {        
        System.out.println("f(int x, String s)");       
    }

    protected void test() {
        System.out.println("protected MyOverloading.test()");
    }

    // BAD PRACTICE !!!
    // 
    // private void f(String s, int x) {        
    // } 
    
    public MyOverloading() {
        System.out.println("default constructor MyOverloading()");
    }

    public MyOverloading(String s) {
        this(s, 0);
        System.out.println("constructor MyOverloading(Strinf s)");
    }

    public MyOverloading(int x) {
        this(null, x);
        System.out.println("constructor MyOverloading(int x)");
    }

    public MyOverloading(String s, int x) {
        this.s = s;
        this.x = x;
        System.out.println("constructor MyOverloading(String s, int x)"); 
    }

    public static void main(String[] args) {
        // MyOverloading obj1 = new MyOverloading();
        // MyOverloading obj2 = new MyOverloading("Hi");
        // MyOverloading obj3 = new MyOverloading(1);
        // MyOverloading obj4 = new MyOverloading("Test", 2);
        
        MyExtraOverloading obj5 = new MyExtraOverloading("String");
        obj5.test();
    }
}

class MyExtraOverloading extends MyOverloading {

    public MyExtraOverloading() {
        super(null,0);
        System.out.println("default constructor MyExtraOverloading()");
    }

    public MyExtraOverloading(String s) {
        super(s,0);
        System.out.println("constructor MyExtraOverloading(String s)");
    }

    @Override
    public void test() {
        // super.test();
        System.out.println("fired MyExtraOverloading.test()");
    }
}