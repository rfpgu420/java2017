import  java.util.List;
import  java.util.ArrayList;
import  java.util.logging.Logger;

public class LogWriter {
    
    private static final Logger LOGGER = Logger.getLogger( LogWriter.class.getName() );

    private List<Integer> obtainSomehow() {
        List<Integer> li = new ArrayList<>();
        li.add(1);
        li.add(3);
        li.add(5);
        return li;
    }

    private void writeLog() {
        List<Integer> listOfIntegers = obtainSomehow();

        listOfIntegers.remove(1);

        System.out.println("listOfIntegers.get(1): "+listOfIntegers.get(1));

        for (Integer i : listOfIntegers) {
            LOGGER.severe("Integer value is : " + i);
        }
    }

    public static void main(String[] args) {
        LogWriter lw = new LogWriter();
        lw.writeLog();
    }
}