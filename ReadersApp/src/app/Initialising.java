public class Initialising {
    
    private String  s;

    private byte    b;
    private char    ch;
    private short   sh;
    private int     i;
    private long    l;
    private float   f;
    private double  d;
    private boolean bb;

    {
        System.out.println("Instance initialization 1 start");
        System.out.println("s = " +s);
        System.out.println("b = " +b);
        System.out.println("ch = "+ch);
        System.out.println("sh = "+sh);
        System.out.println("i = " +i);
        System.out.println("l = " +l);
        System.out.println("f =  "+f);
        System.out.println("d = " +d);
        System.out.println("bb = "+bb);
        System.out.println("Instance initialization 1 end");
    }

    Initialising() {
        System.out.println("default constructor Initialising()");
        s  = "string";
        b  = (byte)1;
        ch = 'c';
        sh = (short)sh;
        i  = 4;
        l  = 77L;
        f  = 7.0F;
        d  = 88.0D;
        bb = true;
    }

    Initialising(String s) {
        // this();
        System.out.println("parameterised constructor Initialising()");
        this.s = s;
    }


    {
        String x = "test string";

        System.out.println("Instance initialization 2 start");
        System.out.println(toString());

        x = test(x);
        System.out.println(x);

        System.out.println("Instance initialization 2 end");
    }

    private String test(String s) {
        return "this is a test string: "+s;
    }

    public String toString() {
        return  "s = " +s 
        +       ", b = " +b
        +       ", ch = "+ch
        +       ", sh = "+sh
        +       ", i = " +i
        +       ", l = " +l
        +       ", f =  "+f
        +       ", d = " +d
        +       ", bb = "+bb;
    }

    public static void main(String[] args) {
        System.out.println(new ExtraInitialising("non-empty string"));
        // ExtraInitialising.printMe();
        
        System.gc();
    }

    static {
        System.out.println("Class Initialising: initialization 1");
    }


    // private void f() {}

    private int f() {
        return 1;
    }

    private int f(int x, String s) {
        return 1;
    }

    private int f(String s, int x) {
        return 1;
    }

    public void print() {
        System.out.println(s);
    }
}


class ExtraInitialising extends Initialising {
    
    private String s;

    ExtraInitialising() {
        System.out.println("default constructor ExtraInitialising()");
    }

    ExtraInitialising(String s) {
        // super(s);
        System.out.println("parameterised constructor ExtraInitialising()");
        // super(s);
        this.s = s;

        super.print();
    }

    public static void printMe() {
        System.out.println("ExtraInitialising.printMe()");
    }

    static {
        System.out.println("Class ExtraInitialising: initialization 1");
    }

    // public String toString() {
    //     return s;
    // }
    
    public void finalize() {
        // finalise-ready condition here
        // if () {
        //  ...
        // }
        
        System.out.println("ExtraInitialising.finalise()");
        try {
            super.finalize();
        }
        catch(Throwable ex) {
            ex.printStackTrace();
        }
    }
}