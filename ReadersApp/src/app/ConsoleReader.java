import static java.lang.System.*;

public class ConsoleReader {

    public static void main(String[] args)
    {
        String input;
        while (true) {
            out.print("Enter something : ");
            input = console().readLine();

            if ("q".equals(input)) {
                out.println("Exit!");
                exit(0);
            }

            out.println("input : " + input);
            out.println("-----------\n");
        }
    }
}