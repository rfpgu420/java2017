@echo off
if exist .\bin\app (
    rd /s /q .\bin\app
)
md .\bin\app

dir /s /B src\app\*.java > tmp\sources.txt
javac -Xlint:deprecation -Xlint:unchecked -cp ".;src/;lib/*" -g -d bin\app @tmp\sources.txt
pause