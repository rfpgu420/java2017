public class MyHelloWorld {
    
    public static void main(String[] args)
    {
        System.out.println("Hello World!");

        System.out.println("PARAMETERS:");        
        int len = args.length;
        for (int i=0; i < len; i++) {
            System.out.println("args["+i+"] = "+args[i]);            
        }
    }
}