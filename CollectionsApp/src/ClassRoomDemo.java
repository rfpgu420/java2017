import java.util.*;
import java.util.logging.Logger;

enum Gender {
    MALE, FEMALE;
}

class Student implements Comparable<Student> {
    private String firstName;
    private String lastName;
    private Gender gender;
    private int    age;

    private Student(){}

    public String getFirstName(){ return firstName; }
    public String getLastName() { return lastName; }
    public Gender getGender()   { return gender; }
    public int    getAge()      { return age; }

    @Override
    public int compareTo(Student o) {
        String s = o.getLastName()+o.getFirstName();
        return s.compareToIgnoreCase(lastName+firstName);
    }

    public int compareByAgeTo(Student o) {
        if (o.getAge() <  age) return -1;
        if (o.getAge() == age) return 0;
        return 1;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(firstName);
        sb.append(" ");
        sb.append(lastName);
        sb.append(", ");
        sb.append(gender);
        sb.append(", ");
        sb.append(age);
        return sb.toString();
    }

    public static Builder newBuilder(Student student) {
        return student.new Builder();
    }

    public static Builder newBuilder() {
        return new Student().new Builder();
    }

    public class Builder {

        private Builder() {
            // private constructor
        }

        public Builder setFirstName(String firstName) {
            Student.this.firstName = firstName;
            return this;
        }

        public Builder setLastName(String lastName) {
            Student.this.lastName = lastName;
            return this;
        }

        public Builder setGender(Gender gender) {
            Student.this.gender = gender;
            return this;
        }

        public Builder setAge(int age) {
            Student.this.age = age;
            return this;
        }

        public Student build() {
            return Student.this;
        }

    }
}

class ClassRoom implements Iterable<Student> {
    
    private static final Logger LOGGER = Logger.getLogger(
        ClassRoom.class.getName()
    );

    private LinkedList<Student> learners = new LinkedList<>();

    /**
     * [clear description]
     */
    public void clear() {
        // LOGGER.info("ClassRoom.clear()");
        learners.clear();
    }

    /**
     * [add description]
     * @param stud [description]
     */
    public void add(Student stud) {
        // LOGGER.info("ClassRoom.add()");

        stud = Student.newBuilder(stud)
            .setLastName (stud.getLastName().toUpperCase())
            .setFirstName(stud.getFirstName().toUpperCase())
            .build();

        learners.add(stud);
    }

    /**
     * [addAll description]
     * @param stud [description]
     */
    public void addAll(Student... stud) {
        // LOGGER.info("ClassRoom.addAll()");
        for(Student st: stud) {
            add(st);
        }
        // learners.addAll(Arrays.asList(stud));
    }

    /**
     * [iterator description]
     * @return [description]
     */
    public Iterator<Student> iterator() {
        return new Iterator<Student>() {
            private Iterator<Student> it = learners.iterator();

            public boolean hasNext(){
                // LOGGER.info("Iterator.hasNext()");
                return it.hasNext();
            }
            
            public Student next() {
                // LOGGER.info("Iterator.Next()");
                return it.next();
            }

            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }

    /**
     * [alfabetIterator description]
     * @return [description]
     */
    public Iterator<Student> alfabetIterator() {
        return new Iterator<Student>() {
            private Iterator<Student> it;

            {
                LinkedList<Student> tmp = new LinkedList<>(learners);
                Collections.sort(tmp, new Comparator<Student>(){
                    public int compare(Student a, Student b){
                        return b.compareTo(a);
                    }
                });
                it = tmp.iterator();
            }

            public boolean hasNext(){
                // LOGGER.info("Iterator.hasNext()");
                return it.hasNext();
            }
            
            public Student next() {
                // LOGGER.info("Iterator.Next()");
                return it.next();
            }

            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }

    public Iterator<Student> youngIterator() {
        return new Iterator<Student>() {
            private Iterator<Student> it;

            {
                LinkedList<Student> tmp = new LinkedList<>(learners);
                Collections.sort(tmp, new Comparator<Student>(){
                    public int compare(Student a, Student b){
                        // return b.compareByAgeTo(a);
                        if (a.getAge() <  b.getAge()) return -1;
                        if (a.getAge() == b.getAge()) return 0;
                        return 1;
                    }
                });
                it = tmp.iterator();
            }

            public boolean hasNext(){
                // LOGGER.info("Iterator.hasNext()");
                return it.hasNext();
            }
            
            public Student next() {
                // LOGGER.info("Iterator.Next()");
                return it.next();
            }

            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }
}

public class ClassRoomDemo {
    public static void main(String[] args) {
        ClassRoom cr = new ClassRoom();
        cr.add(Student.newBuilder()
            .setLastName("Ivanov")
            .setFirstName("Ivan")
            .setGender(Gender.MALE)
            .setAge(23)
            .build()
        );

        cr.addAll(
            Student.newBuilder()
            .setLastName("Petrov")
            .setFirstName("Petr")
            .setGender(Gender.MALE)
            .setAge(21)
            .build(),
            Student.newBuilder()
            .setLastName("Sidorov")
            .setFirstName("Anton")
            .setGender(Gender.MALE)
            .setAge(20)
            .build(),
            Student.newBuilder()
            .setLastName("Vasechkin")
            .setFirstName("Vasiliy")
            .setGender(Gender.MALE)
            .setAge(28)
            .build(),
            Student.newBuilder()
            .setLastName("Petechkin")
            .setFirstName("Petr")
            .setGender(Gender.MALE)
            .setAge(30)
            .build()
        );

        System.out.println("non-sorted list:");
        for(Student st: cr) {
            System.out.println(st);
        }
        
        System.out.println();
        System.out.println("sorted list:");
        // Iterator<Student> it = cr.alfabetIterator();
        Iterator<Student> it = cr.youngIterator();

        while(it.hasNext()) {
            System.out.println(it.next());
        }
    }
}