import java.util.*;

public class UseArrayListDemo {

    /**
     * 
     */
    static interface CanFire {
        void fire();
    }

    /**
     * 
     */
    static interface CanDestroy {
        void destroy();
    }

    /**
     * 
     */
    static interface CanDoMagic {
        void magic();
    }

    /**
     * 
     */
    static abstract class GameCharacter {
        @Override public String toString() {
            return getClass().getSimpleName();
        }
    }

    /**
     * 
     */
    static class Hero extends GameCharacter
                      implements CanFire
    {
        public void fire() {
            System.out.println(getClass().getSimpleName()+".fire()");
        }
    }

    static class Wizard extends Hero
                        implements CanDoMagic
    {
        public void magic() {
            System.out.println(getClass().getSimpleName()+".magic()");
        }
    }

    /**
     * 
     */
    static class Monster extends GameCharacter
                         implements CanDestroy
    {
        public void destroy() {
            System.out.println(getClass().getSimpleName()+".destroy()");
        }
    }

    static public void printInts(int... x) {
        for(int i: x) {
            System.out.println("i = "+i);
        }
    }

    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        ArrayList characters = new ArrayList();
        characters.add(new Hero());
        characters.add(new Hero());
        characters.add(new Monster());

        // for(int i=0; i<characters.size(); i++) {
        //     System.out.println(characters.get(i));
        //     // ((Hero)characters.get(i)).fire();
        // }


        ArrayList<Hero> characters2 = new ArrayList<>();
        characters2.add(new Hero());
        characters2.add(new Hero());
        characters2.add(new Wizard());
        characters2.add(new Wizard());
        // characters2.add(new Monster());
        
        // for(Hero h: characters2) {
        //     System.out.println("do action:");
        //     h.fire();
        // }
        
        printInts(0,1);
        printInts(2,3,4,5,6,7,8);
        printInts(9,10,11,12);
    }
}