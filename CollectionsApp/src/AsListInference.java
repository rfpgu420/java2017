import java.util.*;

public class AsListInference {

    static class Snow{}
    static class Powder extends Snow {}
    static class Light  extends Powder {}
    static class Heavy  extends Powder {}
    static class Crusty extends Snow {}
    static class Slush  extends Snow {}

    public static void main(String[] args) {
        List<Snow> snow1 = Arrays.asList(
            new Crusty(), new Slush(), new Powder()
        );

        // List<Snow> snow2 = Arrays.asList( // new ArrayList<Powder>()
        //     new Light(), new Heavy()
        // );
        // 
        // error: incompatible types
        // 
        // required: List<Snow>
        // found:    List<Powder>

        List<Snow> snow3 = new ArrayList<>();
        Collections.addAll(snow3, new Light(), new Heavy());

        List<Snow> snow4 = Arrays.<Snow>asList(
            new Light(), new Heavy()
        );
    }
}

