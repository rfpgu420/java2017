import java.util.*;

public class IteratorDemo01 {
    
    static class MagicWords implements Iterable<String> {
        private String[] words = {
            "fire", "action", "success",
            "power", "FMI4EVER", "cool",
            "JAVA", "PYTHON", "PHP", "C#"
        };

        public Iterator<String> iterator() {
            return new Iterator<String>(){
                private int current = 0;
                private int size    = words.length;

                public boolean hasNext(){
                    return current < size;
                }
                
                public String  next() {
                    return words[current++];
                }

                public void    remove() {
                    throw new UnsupportedOperationException();
                }
            };
        }

        public Iterator<String> reverseIterator() {
            return new Iterator<String>(){
                private int current = words.length - 1;
                private int size    = words.length;

                public boolean hasNext(){
                    return current >= 0;
                }
                
                public String  next() {
                    return words[current--];
                }

                public void    remove() {
                    throw new UnsupportedOperationException();
                }
            };
        }

        public Iterator<String> even_iterator() {
            return new Iterator<String>(){
                private int current = 0;
                private int size    = words.length;


                public boolean hasNext(){
                    return current < size;
                }
                
                public String  next() {
                   int old_cur=current;
                    current+=2;
                    if (old_cur>=size)
                        return null;
                    return words[old_cur];

                }

                public void    remove() {
                    throw new UnsupportedOperationException();
                }
            };
        }


    }


    public static void main(String[] args) {
        MagicWords mw = new MagicWords();
        /*for(String s: mw) {
            System.out.println(s);
        }*/
        Iterator iter=mw.even_iterator();
        while (iter.hasNext()){
            System.out.println(iter.next());

        } 
        }
}