import java.util.*;

public class GenericsAndUpcating {

    static class GrannySmith extends Apple {
    }
    
    static class Gala extends Apple {
    }

    static class Fuji extends Apple {
    }

    static class Braeburn extends Fuji {
    }


    public static void main(String[] args) {
        // ArrayList<Apple> apples = new ArrayList<>();
        List<Apple> apples = new ArrayList<>();
        
        apples.add(new GrannySmith());
        apples.add(new Gala());
        apples.add(new Fuji());
        apples.add(new Braeburn());

        for (Apple a: apples) {
            System.out.println(a.getClass().getSimpleName());
        }
    }
}