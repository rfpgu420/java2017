public class UseArraysDemo {
    
    static class MyLongList {
        private long[] longs;

        public MyLongList(long[] longs) {
            this.longs = longs; 
        }

        public MyLongList(int size) {
            longs = new long[size]; 
        }
    }


    public static void main(String[] args) {
        int[] ints  = new int[10];
        int[] ints2 = {0,1,2,3,4,5,6,7,8,9};
        int[] ints3 = new int[]{
            0,1,2,3,4,5,6,7,8,9
        };

        for(int i=0; i<ints.length; i++) {
            System.out.print(
                ints[i]+(i==(ints.length-1) ? "" : ", ")
            );
        }

        System.out.println();
        for(int i: ints2) {
            System.out.print(i+", ");
        }

        MyLongList mylongs  = new MyLongList(10);
        MyLongList mylongs2 = new MyLongList(
            new long[]{0,1,2,3,4,5}
        );
    }
}