import java.util.*;

public class PrintingContainers {

    static Collection fill(Collection<String> collection) {
        collection.add("mouse");
        collection.add("cat");
        collection.add("dog");
        collection.add("dog");
        return collection;
    }

    static Map fill(Map<String, String> map) {
        map.put("mouse", "Jerry");
        map.put("cat", "Tom");
        map.put("dog", "Snoopy");
        map.put("dog", "Spyke");
        return map;
    }

    static void print(Collection collection) {
        Iterator iterator = collection.iterator();
        System.out.print("[");
        while (iterator.hasNext() ) {
            System.out.print(iterator.next() + (iterator.hasNext() ? ", " : ""));
        }
        System.out.println("]");
    }

    static void print(Map<String, String> map) {
        System.out.print("[");
        for (Map.Entry e : map.entrySet()) {
            System.out.print( e.getKey() + "=" + e.getValue() + ", " );
        }
        System.out.println("]");
    }

    public static void main(String[] args) {
        print(fill(new ArrayList<String>()));
        print(fill(new LinkedList<String>()));
        print(fill(new HashSet<String>()));
        print(fill(new TreeSet<String>()));
        print(fill(new LinkedHashSet<String>()));
        print(fill(new HashMap<String,String>()));
        print(fill(new TreeMap<String,String>()));
        print(fill(new LinkedHashMap<String,String>()));
    }
}