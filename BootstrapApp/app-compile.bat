@echo off
set "home=."
set "dist=%home%/dist"
set "libs=%dist%/libraries/*"
set "cp=%home%;%libs%"
@echo on
javac -Xlint:deprecation -Xlint:unchecked -cp "%cp%" -g -d "%dist%"         src/Bootstrap.java
javac -Xlint:deprecation -Xlint:unchecked -cp "%cp%" -g -d "%dist%"         src/ClassA.java
javac -Xlint:deprecation -Xlint:unchecked -cp "%cp%" -g -d "%dist%/commons" src/Application.java
pause