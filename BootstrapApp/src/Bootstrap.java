import java.io.File;
import java.io.FileFilter;
import java.io.FilePermission;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

import java.security.Permission;
import java.security.AccessController;
import java.security.PrivilegedAction;


/**
 * You can try this code out for yourself:
 *
 * javac -cp . Bootstrap.java
 * java  -cp . Bootstrap Applicaton Hello!
 *     
 */
public class Bootstrap {

    private static final String HOME    = "./dist";
    private static final String COMMONS = HOME + "/commons";
    private static final String PLUGINS = HOME + "/plugins";
    private static final String LIB     = HOME + "/libraries";

    private static ClassLoader commonsClassLoader;
    private static ClassLoader pluginsClassLoader;
    
    static {
        List<URL> urls = new ArrayList<>();
        try {
            for (File f : new File(PLUGINS).listFiles(
                new FileFilter() {
                    public boolean accept(File file) {
                        return file.isFile() 
                            && file.getName().toLowerCase().endsWith(".jar");
                    }
                })
            ){
                urls.add(f.toURI().toURL());
            }

            pluginsClassLoader = new URLClassLoader(
                urls.<URL>toArray(new URL[0]),
                null /*ClassLoader.getSystemClassLoader().getParent()*/
            );
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    static {
        // add the classes dir and each jar in lib to a List of URLs.
        List<URL> urls = new ArrayList<>();
        try {
            urls.add(new File(COMMONS).toURI().toURL());
            for (File f : new File(LIB).listFiles()) {
                urls.add(f.toURI().toURL());
            }

            // feed your URLs to a URLClassLoader!
            commonsClassLoader = new URLClassLoader(
                urls.<URL>toArray(new URL[0]),
                null /* ClassLoader.getSystemClassLoader().getParent()*/
            );            
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception
    {
        // relative to that classloader, find the main class
        // you want to bootstrap, which is the first cmd line arg
        Class<?> mainClass = commonsClassLoader.loadClass(args[0]);
        final Method main = mainClass.getMethod("main", new Class[]{args.getClass()});

        // well-behaved Java packages work relative to the
        // context classloader. Others don't (like commons-logging)
        Thread.currentThread().setContextClassLoader(/*pluginsClassLoader*/ commonsClassLoader);

        // you want to prune the first arg because its your main class.
        // you want to pass the remaining args as the "real" args to your main
        final String[] nextArgs = new String[args.length - 1];
        System.arraycopy(args, 1, nextArgs, 0, nextArgs.length);
        // main.invoke(null, new Object[] { nextArgs });

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                SecurityManager old = System.getSecurityManager();
                PluginSecurityManager psm = new PluginSecurityManager();
                System.setSecurityManager(psm);
                psm.enableSandbox();

                runUntrustedCode();

                psm.disableSandbox();
                System.setSecurityManager(old);
            }

            private void runUntrustedCode() {
                try {
                    // run the custom class's main method for example:
                    main.invoke(null, new Object[] { nextArgs });    
                }
                catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        });
        // thread.setContextClassLoader(/*pluginsClassLoader*/ commonsClassLoader);
        thread.start();
    }

    public static class PluginSecurityManager extends SecurityManager {
        private String  _classSource;
        private boolean _sandboxed;

        void enableSandbox() {
            _sandboxed = true;
        }

        void disableSandbox() {
            _sandboxed = false;
        }

        @Override
        public void checkPermission(Permission perm) {
            // super.checkPermission(perm);
            // check(perm);
        } 

        @Override
        public void checkPermission(Permission perm, Object context) {
            // super.checkPermission(perm, context);
            // check(perm);
        }

        private void check(Permission perm) {
            // if (_classSource == null) {
            //     // Not running plugin code
            //     return;
            // }
            
            if (!_sandboxed) {
                return;
            }

            if (perm instanceof FilePermission)
            {
                // Is the request inside the class source?
                String path = perm.getName();
                boolean inClassSource = path.startsWith(_classSource);

                // Is the request for read-only access?
                boolean readOnly = "read".equals(perm.getActions());

                if (inClassSource && readOnly) {
                    return;
                }
            }

            throw new SecurityException("Permission denied: " + perm);
        }

        void setClassSource(String classSource) {
            _classSource = classSource;
        }
    }
}