import java.util.Arrays;
import java.net.URL;
import java.security.AccessController;
import java.security.PrivilegedAction;

/**
 * @link  https://github.com/junit-team/junit4/wiki/Assertions
 */
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.both;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.everyItem;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.hamcrest.core.CombinableMatcher;
import org.junit.Test;

public class Application {

    private void run()
    {
        System.out.println("The Application.main() method fired!");

        AccessController.doPrivileged(new PrivilegedAction<Void>() {
            public Void run() {
                // privileged code goes here, for example:
                URL classLocation = getClass().getProtectionDomain().getCodeSource().getLocation();
                System.out.println("application main class location: "+classLocation);

                return null; // nothing to return
            }
        });

        URL classLocation = org.junit.Test.class.getProtectionDomain().getCodeSource().getLocation();
        System.out.println("application plugins location: "+classLocation);

        System.out.println("thread  context classloader: "+Thread.currentThread().getContextClassLoader());
        System.out.println("current class   classloader: "+getClass().getClassLoader());
        
        try {
            // ClassLoader classloader = Application.class.getClassLoader();
            // Class<?>    clazz = classloader.loadClass("ClassA");
            // System.out.println(clazz.getClassLoader());
            
            testAssertArrayEquals();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new Application().run();       
    }

    @Test
    public void testAssertArrayEquals() {
        byte[] expected = "trial".getBytes();
        byte[] actual = "trial".getBytes();
        assertArrayEquals("failure - byte arrays not same", expected, actual);
    }
}