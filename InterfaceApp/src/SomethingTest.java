abstract class Something {
    
    abstract protected void f();

    public void z() {
        System.out.println(getClass().getSimpleName()+".z()");
    }
}

class ThingOne extends Something {

    @Override
    protected void f() {
        System.out.println("ThingOne.f()");
    }

    public static void g(Something obj) {
        // ((ThingOne)obj).f(); // Something.f() defined without key word "abstract"
        obj.f();
        // obj.f();                // Something.f() defined with key word "abstract"
    }
}

public class SomethingTest {

    public static void main(String[] args) {
        Something o = new ThingOne();
        ThingOne.g(o);
    }
}