package fabric;

import  java.util.logging.*;
import  java.util.Random;

enum Coin {
    TAIL, EAGLE;
}

enum Dice {
    ONE, TWO, THREE, FOUR, FIVE, SIX;
}

interface Service {
    Logger LOGGER = Logger.getLogger(Service.class.getName());
    void fling();
}

interface ServiceFactory {
    Service getService();
}

class CoinFlingService implements Service {
    CoinFlingService(){}
    
    public void fling(){
        // LOGGER.info(getClass().getSimpleName() + ".fling()");
        int i = ((int)(Math.random()*10)) % Coin.values().length;
        System.out.println(Coin.values()[i]);
    }
}

class CoinFlingFactory implements ServiceFactory {
    public Service getService() {
        return new CoinFlingService();
    }
}

class DiceFlingService implements Service {
    DiceFlingService(){}

    public void fling(){
        // LOGGER.info(getClass().getSimpleName() + ".fling()");
        int i = ((int)(Math.random()*10)) % Dice.values().length;
        System.out.println(Dice.values()[i]);
    }
}

class DiceFlingFactory implements ServiceFactory {
    public Service getService() {
        return new DiceFlingService();
    }
}


public class Game {

    private static void serviceConsumer(ServiceFactory factory) {
        Service service = factory.getService();
        service.fling();
    }

    public static void main(String[] args) {
        serviceConsumer(new CoinFlingFactory());
        serviceConsumer(new DiceFlingFactory());
    }
}