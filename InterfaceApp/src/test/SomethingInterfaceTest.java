package test;

interface Something {
    
    void f();

    void z();
}

interface SomethingElse extends Something {

    void y();
}

class ThingOne implements SomethingElse {

    @Override
    public void f() {
        System.out.println("ThingOne.f() from Something");
    }
    
    @Override
    public void z() {
        System.out.println("ThingOne.z() from Something");
    }

    @Override
    public void y() {
        System.out.println("ThingOne.y() from SomethingElse");
    }

    public static void g(Something obj) {
        obj.f();
    }
}

public class SomethingInterfaceTest {

    public static void main(String[] args) {
        // Something o = new ThingOne();
        // ThingOne.g(o);
        
        Something[] things = {
            new ThingOne(), new ThingOne(),
            new ThingOne(), new ThingOne()
        };

        for (Something t : things) {
            t.f();
            t.z();
            ((SomethingElse)t).y();
        }
    }
}