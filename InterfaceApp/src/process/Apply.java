package process;

abstract class Processor implements Processable {
    
    public String name() {
        return getClass().getSimpleName();
    }

    public Object process(Object input) {
        return input;
    }
}

class Upcase extends Processor {
    public String process(Object input) {
        return ((String) input).toUpperCase();
    }
}

class Downcase extends Processor {
    public String process(Object input) {
        return ((String) input).toLowerCase();
    }
}

class Splitter extends Processor {
    public String process(Object input) {
        return java.util.Arrays.toString(((String) input).split(" "));
    }
}

public class Apply {
    
    // Pattern "Strategy"
    public static void process(Processable p, Object o) {
        System.out.printf("Using Processor %1$s \n", p.name());
        System.out.println(p.process(o));
    }

    public static void main(String[] args) {
        String s = "Disagreemnet with beliefs is by definition incorrect.";
        process(new Upcase(), s);
        process(new Downcase(), s);
        process(new Splitter(), s);
    }
}