package process;

abstract class Order implements Processable {
    public String name() {
        return getClass().getSimpleName();
    }

    public Object process(Object input) {
        return input;
    }
}

class SpecialOrder1 extends Order {
    public String name() {
        return getClass().getSimpleName();
    }

    public String process(Object input) {
        return doAction();
    }

    private String doAction() {
        return "Complete";
    }
}

class SpecialOrder2 extends Order {
    public String name() {
        return getClass().getSimpleName();
    }

    public String process(Object input) {
        return doAction();
    }

    private String doAction() {
        return "Processing";
    }

}

public class OrdersTest {
    
    public static void main(String[] args) {
        Object o = new Object();
        Apply.process(new SpecialOrder1(), o);
        Apply.process(new SpecialOrder2(), o);
    }
}