package process;

public interface Processable {

    String name();
    
    Object process(Object input);
} 