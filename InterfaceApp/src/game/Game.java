package game;

import  java.util.logging.*;

interface Movable {
    Logger LOGGER = Logger.getLogger(
        Movable.class.getName()
    );

    void move();
}

interface AbleToDie {
    void die();
}

interface Attackable {
    void attack(AbleToDie object);
}

interface AbleToMagic {
    void magic(Movable object);
}

interface CanFly {
    void fly();
}

interface CanHide {
    void hide(int seconds);
}

interface Immortal extends CanHide, CanFly, AbleToMagic
                         , Attackable, Movable {
}

abstract class Hero implements Movable, AbleToDie {
    // abstract public void move();
    // abstract public void die();
}

class Warrior extends Hero implements Attackable {
    public void move() {
        LOGGER.severe(getClass().getSimpleName() + ".move()");
    }
    public void die() {
        LOGGER.severe(getClass().getSimpleName() + ".die()");
    }
    public void attack(AbleToDie object) {
        LOGGER.severe(getClass().getSimpleName() + ".attack()");
    }
}

class Wizard extends Hero implements AbleToMagic, Attackable {
    public void move() {
        LOGGER.info(getClass().getSimpleName() + ".move()");
    }
    public void die() {
        LOGGER.info(getClass().getSimpleName() + ".die()");
    }
    public void magic(Movable object) {
        LOGGER.info(getClass().getSimpleName() + ".magic()");
    }
    public void attack(AbleToDie object) {
        LOGGER.info(getClass().getSimpleName() + ".attack()");
    }
}

abstract class Superman implements Immortal {

}

abstract class Monster implements Movable, Attackable, AbleToDie {
    // abstract public void move();
    // abstract public void die();
    // abstract public void attack(AbleToDie object);
}

class FlyMonster extends Monster implements CanFly {
    public void move() {
        LOGGER.info(getClass().getSimpleName() + ".move()");
    }
    public void die() {
        LOGGER.info(getClass().getSimpleName() + ".die()");
    }
    public void attack(AbleToDie object) {
        LOGGER.info(getClass().getSimpleName() + ".attack()");
    }
    public void fly() {
        LOGGER.info(getClass().getSimpleName() + ".fly()");
    }
}

class HiddenMonster extends Monster implements CanHide {
    public void move() {
        LOGGER.info(getClass().getSimpleName() + ".move()");
    }
    public void die() {
        LOGGER.info(getClass().getSimpleName() + ".die()");
    }
    public void attack(AbleToDie object) {
        LOGGER.info(getClass().getSimpleName() + ".attack()");
    }
    public void hide(int seconds) {
        LOGGER.info(getClass().getSimpleName() + ".hide()");
    }
}

interface CanSpeak {
    void speak();
}

abstract class Thing {
    abstract void display();
}

class Stone extends Thing implements Movable, CanFly, CanSpeak {
    public void display() {
        LOGGER.info(getClass().getSimpleName() + ".display()");
    }
    public void move() {
        LOGGER.info(getClass().getSimpleName() + ".move()");
    }
    public void fly() {
        LOGGER.info(getClass().getSimpleName() + ".fly()");
    }
    public void speak() {
        LOGGER.info(getClass().getSimpleName() + ".speak()");
    }
}

public class Game {
    public static void main(String[] args) {
        Movable[] objects = {
            new Warrior(), new Warrior(), new Wizard(),
            new FlyMonster(), new HiddenMonster(),
            new HiddenMonster(),new HiddenMonster(),
            new Stone(), new Stone()
        };

        for (Movable m : objects) {
            m.move();
        }

        Movable.LOGGER.info(Game.class.getSimpleName() + ".main()");
    }
}