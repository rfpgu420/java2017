package strategy;



public class ProcessArray {

    private static interface Processable {
        
    }

    private static int[][] elements = {
        {78, 89, 34, 12, 45, 23, 9, 1, 56},
        {37, 29, 23, 12, 2, 89, 30, 27},
        {67, 39, 3, 9, 18, 36, 15},
        {99, 24},
        {76, 82, 35, 39, 26, 71, 4},
        {29, 58, 41, 93, 85, 11, 78, 29}
    };

    private static void process(Processable p) {

    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Error! Valid usages:");
            String message = "\njava ProcessArray %1$s";
            System.out.printf(message,"min");
            System.out.printf(message,"max");
            System.out.printf(message,"sum");
            return;
        }
        process(args[0]);
    }
}