abstract class PrintBase {
    
    public PrintBase() {
        print();
    }

    abstract public void print();
}

class PrintOne extends PrintBase {
    private int x = 1;

    public void print() {
        System.out.println("PrintOne.print(): x = " + x);
    }
}

class PrintTwo extends PrintBase {
    private int x = 2;

    public void print() {
        System.out.println("PrintTwo.print(): x = " + x);
    }
}


public class PrintTest {
    
    public static void main(String[] args) {
        PrintOne p1 = new PrintOne();
        p1.print();

        PrintTwo p2 = new PrintTwo();
        p2.print();
    }
}