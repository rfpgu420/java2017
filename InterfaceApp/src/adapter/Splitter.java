package adapter;

public class Splitter extends Processor {
    public String process(Object input) {
        return java.util.Arrays.toString(((String) input).split(" "));
    }
}