package adapter;

interface Processable {
    String name();
    Object process(Object input);
}