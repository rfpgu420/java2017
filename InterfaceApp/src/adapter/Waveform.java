package adapter;

public class Waveform {
    private static long counter;
    private final  long id = counter++;

    public String toString(){
        return  String.format("%1$s: %2$d",
            getClass().getSimpleName(), id
        );
    }
}