package adapter;

public class ProcessorAdapter implements Processable {
    
    protected Processor proc;

    public ProcessorAdapter(Processor proc) {
        this.proc = proc;
    }

    public String name() {
        return proc.name();
    }

    public String process(Object input) {
        return (String)proc.process(input);
    }
}
