package adapter;

public class Apply {
    
    // Pattern "Strategy"
    public static void process(Processable p, Object o) {
        System.out.printf("Using Processor %1$s \n", p.name());
        System.out.println(p.process(o));
    }

    public static void main(String[] args) {
        String s = "Disagreemnet with beliefs is by definition incorrect.";
        process(new ProcessorAdapter(new Upcase()), s);
        process(new ProcessorAdapter(new Downcase()), s);
        process(new ProcessorAdapter(new Splitter()), s);

        Waveform w = new Waveform();
        process(new FilterAdapter(new LowPass(1.0)), w);
        process(new FilterAdapter(new HighPass(2.0)), w);
        process(new FilterAdapter(new BandPass(3.0, 4.0)), w);
    }
}