public class Trigonometric{
	public static double factorial(double numTerms){

		if (numTerms == 0) return 1;
		return numTerms * factorial(numTerms - 1);

	}

	public static double sin(int x, int numTerms){

		double z = 3;

		double s = x;
		System.out.println(s);

		for (int i = 1; i <= numTerms; i++) {
			s -= ((Math.pow(x,z)) / factorial(z)) + ((Math.pow(x,z+=2)) / factorial(z+=2));
			z += 4;
			System.out.println(s);
		}

		System.out.print("answer: ");
		return s;

	}

	public static double cos(int x, int numTerms){

		double z = 2;

		double c = 1;
		System.out.println(c);

		for (int i = 1; i <= numTerms; i++) {
			c -= ((Math.pow(x,z)) / factorial(z)) + ((Math.pow(x,z+=2)) / factorial(z+=2));
			z += 4;
			System.out.println(c);
		}

		System.out.print("answer: ");
		return c;

	}

	public static void main(String[] args) {
		int rand_num = (int)(Math.random() * 5);
		int x = (int)(Math.random() * 10);
		System.out.println(sin(x,rand_num));
		System.out.println("the value of X = " + x);
		System.out.println("number of iterations = " + rand_num);
		System.out.println("------------------------------");
		System.out.println(cos(x,rand_num));
		System.out.println("the value of X = " + x);
		System.out.println("number of iterations = " + rand_num);
	}
}