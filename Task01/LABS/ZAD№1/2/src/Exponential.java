public class Exponential{
	public static double factorial(double numTerms){

		if (numTerms == 0) return 1;
		return numTerms * factorial(numTerms - 1);

	}

	public static double exp(int numTerms) {

		double e = 1;
		System.out.println(e);

		for (double i = 1; i <= numTerms; i++) {
			e += (1 / factorial(i));
			System.out.println(e);			
		}

		System.out.print("answer: ");
		return e;
	}

	public static double exp_x(int x, int numTerms){

		double e = 1;
		System.out.println(e);

		for (double i = 1; i <= numTerms; i++) {
			e += ((Math.pow(x, i)) / factorial(i));
			System.out.println(e);			
		}

		System.out.print("answer: ");
		return e;

	}

	public static void main(String[] args) {
		int rand_num = (int)(Math.random() * 5);
		int x = (int)(Math.random() * 10);
		System.out.println(exp(rand_num));
		System.out.println("number of iterations = " + rand_num);
		System.out.println("------------------------------");
		System.out.println(exp_x(x,rand_num));
		System.out.println("the value of X = " + x);
		System.out.println("number of iterations = " + rand_num);	
	}
}