public class Special{
	public static double sumOfSeries(int x, int numTerms){

		double z = 3;
		double pr1 = 1;
		double pr2 = 2;

		double sum = x;
		System.out.println(sum);

		for (int i = 1; i <= numTerms; i++) {
			sum += (pr1 / pr2) * ((Math.pow(x,z)) / z);
			z += 2;
			pr1 += 1;
			pr2 += 1;
			System.out.println(sum);
		}

		System.out.print("answer: ");
		return sum;

	}

	public static void main(String[] args) {
		int rand_num = (int)(Math.random() * 5);
		int x = (int)(Math.random() * 10);
		System.out.println(sumOfSeries(x,rand_num));
		System.out.println("the value of X = " + x);
		System.out.println("number of iterations = " + rand_num);
	}
}