// java's classes
import java.io.File;
import java.io.FileFilter;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.MalformedURLException;

// app's classes
import org.javatarix.plugin.Plugin;

public class App {
    
    private static File[]   jars;
    private static Class[]  pluginClasses;

    private static void findPlugins() {
        File pluginDir = new File("ext");
        jars = pluginDir.listFiles(new FileFilter() {
            public boolean accept(File file) {
                return file.isFile() && (file.getName().endsWith(".jar") || file.getName().endsWith(".class"));
            }
        });
    }

    private static void loadPlugins()
    {
        pluginClasses = new Class[jars.length];
        for (int i = 0; i < jars.length; i++) {
            try {
                URL jarURL = jars[i].toURI().toURL();
                if (jars[i].getName().endsWith(".class")) {
                    String url =  jarURL.toString();
                    int    indexOfLastDirDelimeter = url.lastIndexOf('/');
                    url    = url.substring(0,indexOfLastDirDelimeter)+'/';
                    jarURL = new URI(url).toURL();
                }
                URLClassLoader classLoader = new URLClassLoader(new URL[]{jarURL});    
                pluginClasses[i] = classLoader.loadClass("HelloPlugin");
            } catch (URISyntaxException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args)
    {
        findPlugins();
        loadPlugins();

        for (Class clazz : pluginClasses) {
            try {
                Plugin instance = (Plugin) clazz.newInstance();
                instance.invoke();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }
}