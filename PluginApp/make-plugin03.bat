@echo off
if exist .\bin\plugin (
    rd /s /q .\bin\plugin
)
md .\bin\plugin

@echo on
dir /s /B src\plugin03\*.java > tmp\sources.txt
javac -Xlint:deprecation -Xlint:unchecked -cp ".;lib/plugin-api.jar" -d bin/plugin @tmp\sources.txt

copy /Y .\bin\plugin\* .\ext\

@echo off
if exist .\bin\plugin (
    rd /s /q .\bin\plugin
)