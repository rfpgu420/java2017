@echo off
if exist .\bin\api (
    rd /s /q .\bin\api
)
md .\bin\api

@echo on
dir /s /B src\api\*.java > tmp\sources.txt
javac -Xlint:deprecation -Xlint:unchecked -cp ".;" -d bin\api @tmp\sources.txt
jar cvfm lib\plugin-api.jar res\plugin-api-manifest.mf -C bin\api .

@echo off
if exist .\bin\api (
    rd /s /q .\bin\api
)