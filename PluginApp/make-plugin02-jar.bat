@echo off
if exist .\bin\plugin (
    rd /s /q .\bin\plugin
)
md .\bin\plugin

@echo on
dir /s /B src\plugin02\*.java > tmp\sources.txt
javac -Xlint:deprecation -Xlint:unchecked -cp ".;lib/plugin-api.jar" -d bin\plugin @tmp\sources.txt
jar cvfm ext\plugin02.jar res\plugin02-manifest.mf -C bin\plugin .

@echo off
if exist .\bin\plugin (
    rd /s /q .\bin\plugin
)